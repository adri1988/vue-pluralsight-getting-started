// mixin.js
import Vue from 'vue';
import Component from 'vue-class-component';

// You can declare a mixin as the same style as components.
@Component
export default class CreatedHookMixin extends Vue {
  // eslint-disable-next-line class-methods-use-this
  created() {
    console.log('Componente creado desde mixin');
  }
}
