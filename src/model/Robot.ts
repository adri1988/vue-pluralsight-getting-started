interface Robot {
    heads: RobotPart [],
    leftArms: RobotPart [],
    torsos: RobotPart [],
    rightArms: RobotPart [],
    bases: RobotPart [],
}
